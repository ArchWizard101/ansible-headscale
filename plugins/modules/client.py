#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
---
module: client

short_description: Manage headscale clients

# If this is part of a collection, you need to use semantic versioning,
# i.e. the version is of the form "2.5.0" and not "2.4".
version_added: "1.0.0"

description: Manage headscale clients

options:
    name:
        description: Name of the user to create
        required: true
        type: str
    state:
        description: Final state of the user
        required: true
        type: str
        choices:
            - present
            - absent

# Specify this value according to your collection
# in format of namespace.collection.doc_fragment_name
extends_documentation_fragment:
    - archwizard.headscale.doc_client

author:
    - Archwizard (@ArchWizard101)
'''

EXAMPLES = r'''
# Pass in a message
- name: Add a new user
    archwizard.headscale.client:
    name: archy
    state: present
- name: Delete a user
    archwizard.headscale.client:
    name: archy
    state: absent
'''

RETURN = r''' # '''

import json
from ansible.module_utils.basic import AnsibleModule

def get_headscale_users(module):
    headscale_cmd = ["headscale", "users", "list", "-o json-line"]
    _, stdout, _ = module.run_command(headscale_cmd, check_rc=True)
    print(stdout)
    output_json = stdout.splitlines()
    output = json.loads(output_json[1])
    result = []
    for i in output:
        result.append(i['name'])

def add_headscale_user(module, username):
    headscale_cmd = ["headscale", "users", "--force", "create", username]
    module.run_command(headscale_cmd, check_rc=True)

def delete_headscale_user(module, username):
    headscale_cmd = ["headscale", "users", "--force", "create", username]
    module.run_command(headscale_cmd, check_rc=True)


def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        name=dict(type='str', required=True),
        state=dict(type='str', choices=["present", "absent"], required=True)
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # changed is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications

    # use whatever logic you need to determine whether or not this module
    # made any modifications to your target
    current_users = get_headscale_users(module)
    if module.params['state'] == "absent":
        if module.params['name'] in current_users:
            result['changed'] = True
            if module.check_mode:
                module.exit_json(**result)
            delete_headscale_user(module, module.params["name"])
    else:
        if module.params['name'] not in current_users:
            result['changed'] = True
            if module.check_mode:
                module.exit_json(**result)
            add_headscale_user(module, module.params["name"])

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()